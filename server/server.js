const express = require('express');
const bodyParser = require('body-parser');

const todosRouter = require('./routes/todos');

const PORT = process.env.PORT || 8000;

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/api/todos', todosRouter);

app.listen(PORT, () => {
    console.log(`server running on port: ${PORT}`);
});

