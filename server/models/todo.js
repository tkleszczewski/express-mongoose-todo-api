const {mongoose} = require('../db/mongoose');

var schema = mongoose.Schema;

let Todo = schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        default: ''
    },
    author: {
        type: String,
        default: ''
    },
    isCompleted: {
        type: Boolean,
        default: false
    }
});

module.exports.Todo = mongoose.model('Todo', Todo);